#include "heap.h"

#include <stdlib.h>
#include <stdio.h>

int cmpfunc_max(int i, int j) {
	return i - j;
}

int cmpfunc_min(int i, int j) {
	return j - i;
}

void heapify(heap_p heap, int inode) {
	int largest;
	int l = HEAP_LCHILD(inode);
	int r = HEAP_RCHILD(inode);

	if (l < heap->size && heap->cmpfunc(heap->tree[l]->data, heap->tree[inode]->data) > 0) 
		largest = l;
	else largest = inode;

	if (r < heap->size && heap->cmpfunc(heap->tree[r]->data, heap->tree[largest]->data) > 0) 
		largest = r;

	if (largest != inode) {
		heap_node_t* temp_node = heap->tree[inode];
		heap->tree[inode] = heap->tree[largest];
		heap->tree[largest] = temp_node;
		heapify(heap, largest);
	}
}

heap_p heap_init (int heapType){
	heap_p heap = (heap_p) malloc(sizeof(heap_t));
	heap->size = 0;
	if(heapType == MAX_HEAP)
		heap->cmpfunc = cmpfunc_max;
	else if(heapType == MIN_HEAP)
		heap->cmpfunc = cmpfunc_min;
	heap->tree = NULL;
	return heap;
}

void heap_destroy(heap_p heap){
	int i;
	for (i = 0; i < heap->size; i++) 
		free(heap->tree[i]);
	
	free(heap->tree);
	free(heap);
}

int heap_insert(heap_p heap, int value) {
	int npos, nppos; // node position and its parent position.
	int temp_data; // holds temp node data for the switch.

	/* Allocate storage for the tree. */
	heap_node_t **temp;
	if ((temp = (heap_node_t**)realloc(heap->tree, (heap->size +1) * sizeof(heap_node_t*))) == NULL)
		return -1;
	else
		heap->tree = temp;

	/* Allocate storage for the node. */
	if ((*(heap->tree + heap->size) = (heap_node_t*)malloc(sizeof(heap_node_t))) == NULL)
		return -1;
	
	heap->tree[heap->size]->data = value;
	npos = heap->size;
	nppos = HEAP_PARENT(npos);

	while(npos > 0 && heap->cmpfunc(heap->tree[nppos]->data, heap->tree[npos]->data) < 0) {
		temp_data = heap->tree[nppos]->data;
		heap->tree[nppos]->data = heap->tree[npos]->data;
		heap->tree[npos]->data = temp_data;
		npos = nppos;
		nppos = HEAP_PARENT(npos);
	}

	// Adjust the size.
	heap->size++;
	return 0;
}

int heap_extract(heap_p heap, int *data) {
	if (heap->size == 0)
		return -1;

	/* Retrieve top node. */
	*data = heap->tree[0]->data;

	/* make backup of last node. */
	int backup = heap->tree[heap->size-1]->data;

	/* Adjust storage size. */
	if (heap->size -1 > 0) {
		heap_node_t **temp;
		free(heap->tree[heap->size-1]); // Free the node.
		/* Remove the node from the tree. */
		if ((temp = (heap_node_t**)realloc(heap->tree, (heap->size -1) * sizeof(heap_node_t*))) == NULL)
			return -1;
		else
			heap->tree = temp;
		heap->size--;
	} else {
		free(heap->tree[0]);
		free(heap->tree);
		heap->tree = NULL;
		heap->size = 0;
		return 0;
	}

	/* Copy the last node to the top. */
	heap->tree[0]->data = backup;

	/* Correct the heap. */
	heapify(heap, 0);

	return 0;
}

void heap_print(heap_p heap) {
	int i;
	for (i = 0; i < heap->size; i++){
		printf("%i: %i \n", i, heap->tree[i]->data);
	}
}