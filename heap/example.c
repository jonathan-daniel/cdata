#include "heap.h"

#include <stdio.h>

int main(int argc, char **argv) {
	heap_p pHeap; // pointer to heap
	pHeap = heap_init(MIN_HEAP); // initialise the heap
	
	int arrSize = 10, i;
	int arr[10] = {4, 2, 5, 7 , 8, 8, 3 ,1 , 10, 23};
	for (i = 0; i < arrSize; i++) {
		heap_insert(pHeap, arr[i]);
	}

	heap_print(pHeap);
	int data = 0; 
	heap_extract(pHeap, &data);
	printf("\n\n %i \n\n", data);

	heap_print(pHeap);
	heap_destroy(pHeap); // free the heap
	return 0;
}
