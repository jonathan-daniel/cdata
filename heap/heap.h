#ifndef __PRIORITY_HEAP_H
#define __PRIORITY_HEAP_H

#define MAX_HEAP 1
#define MIN_HEAP 0

#define HEAP_PARENT(npos) ((int)(((npos) - 1) / 2))
#define HEAP_LCHILD(npos) (((npos) * 2) + 1)
#define HEAP_RCHILD(npos) (((npos) * 2) + 2)

typedef struct heap_node_s {
	int data;
} heap_node_t;

typedef int (*heapcmpfunc)(int, int);
typedef struct heap_s {
	int size;
	heap_node_t **tree;
	heapcmpfunc cmpfunc;
} heap_t, *heap_p;

/* Initialise a heap structure, heapType should be MAX_HEAP or MIN_HEAP. */
heap_p heap_init (int heapType);

/* Destroy the heap. */
void heap_destroy(heap_p heap);

/* Insert a new element (int). */
int heap_insert(heap_p, int value);

/* Extract the top element. */
int heap_extract(heap_p heap, int *data);

/* Returns the size of the heap. */
int heap_size(const heap_p heap);

/* Print the heap array. */
void heap_print(heap_p heap);

#endif /* __PRIORITY_HEAP_H */
