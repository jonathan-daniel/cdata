A collection of common data structures I wrote for use in projects, written in C with the MIT license.

Contains:
**heap:** heap data structure.
**circbuffer:** circular buffer.
**list:** linked list.
**vector:** vector.