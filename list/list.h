#ifndef _C_LIST_H
#define _C_LIST_H

typedef struct list_node {
	void *data;
	struct list_node *next;
} list_node;

/* Create a list (node) and return it.
 * Argument void *data - The data the node will contain.
 */
list_node* list_create(void *data);

/* Free a list. */
void list_destroy(list_node *list);

/* Add node to the end of the list. */
list_node* list_insert_end(list_node *list, void *data);

/* Remove node from the list. */
void list_remove(list_node **list, list_node *node);

#endif /* _C_LIST_H */