#ifndef _CIRC_BUFFER_H
#define _CIRC_BUFFER_H

#include <stdint.h>

typedef struct circular_s {
    int8_t * buffer;
    int length;
    int tail;    
    int head;
    int fill_count;
} circular_t;

/* Create a new circular buffer. */
circular_t* cb_new_buffer(int length);

/* Free buffer and struct. */
void cb_free_buffer(circular_t *cbuffer);

/* Reset the buffer (empty). */
void cb_reset_buffer(circular_t *cbuffer);

/* Check if the buffer is full. */
int cb_is_full(circular_t *cbuffer); /* Returns true (1) or false (0). */

/* Check if the buffer is empty. */
int cb_is_empty(circular_t *cbuffer); /* Returns true (1) or false (0). */

/* Print (debug) the buffer. */
void cb_print_buffer(circular_t *cbuffer);

/* Insert element. */
void cb_insert_element(circular_t *cbuffer, int8_t element);

/* Pull last element. */
int8_t cb_pull_element(circular_t *cbuffer);

#endif /* _CIRC_BUFFER_H */
