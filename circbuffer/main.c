#include "circbuffer.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    /* Allocate the buffer. */
    int buffer_len = 8;
    circular_t *cbuffer = cb_new_buffer(buffer_len);

    /* Inserting and pulling bytes to debug. */
    for (int i = 0; i < 10; i++)
        cb_insert_element(cbuffer, i +1);
    cb_print_buffer(cbuffer);

    cb_pull_element(cbuffer); 
    cb_print_buffer(cbuffer);
    
    /* Check reset. */
    cb_reset_buffer(cbuffer);
    cb_print_buffer(cbuffer);

    /* Free the buffer. */
    cb_free_buffer(cbuffer); 
    return 0;
}