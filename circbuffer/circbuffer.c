#include "circbuffer.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef enum {false, true} bool;

void cb_free_buffer(circular_t *cbuffer) {
    free(cbuffer->buffer);
    free(cbuffer);
}

circular_t* cb_new_buffer(int length) {
    circular_t *cbuffer = (circular_t*) calloc(1, sizeof(circular_t));
    cbuffer->tail = cbuffer->head = cbuffer->fill_count = 0;
    cbuffer->buffer = (int8_t*) malloc(length);
    cbuffer->length = length;
    return cbuffer;
}

void cb_reset_buffer(circular_t *cbuffer) {
    memset(cbuffer->buffer, 0, cbuffer->length);
    cbuffer->head = cbuffer->tail = cbuffer->fill_count = 0;
}

int cb_is_empty(circular_t *cbuffer) {
    if(cbuffer->fill_count == 0)    
        return true;
    else
        return false;
}

int cb_is_full(circular_t *cbuffer) {
    if(cbuffer->fill_count == cbuffer->length)
        return true;
    else
        return false;
}

void cb_print_buffer(circular_t *cbuffer) {
    for(int i = 0; i < cbuffer->length; i++)
        printf("element %i : %i\n", i, cbuffer->buffer[i]); 
    printf("Head: %i Tail: %i fill_count: %i Full: %i\n\n", cbuffer->head, cbuffer->tail, cbuffer->fill_count, cb_is_full(cbuffer));
}

void cb_insert_element(circular_t *cbuffer, int8_t element) {
    if(!cb_is_full(cbuffer)) cbuffer->fill_count++;
    cbuffer->buffer[cbuffer->head] = element;
    if(cb_is_full(cbuffer) && cbuffer->tail == cbuffer->head)
        cbuffer->tail++;
    if(cbuffer->head != cbuffer->length -1)
        cbuffer->head++;
    else 
        cbuffer->head  = 0;
}

int8_t cb_pull_element(circular_t *cbuffer) {
    if(cb_is_empty(cbuffer)) {
        printf("Error: buffer is empty\n");
        return 0;
    }

    int8_t iret = cbuffer->buffer[cbuffer->tail];
    cbuffer->buffer[cbuffer->tail] = 0;
    cbuffer->tail++;
    cbuffer->fill_count--;
    return iret; 
}
